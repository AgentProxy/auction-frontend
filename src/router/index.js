import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import ProductDetails from "../views/ProductDetails";

Vue.use(VueRouter);

const checkUserLoggedIn = (next) => {
  if (!localStorage.getItem("userLoggedIn")) {
    next("login");
  } else {
    next();
  }
};

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Login,
    beforeEnter: (to, from, next) => {
      // Proceed to home page if user is still logged in
      if (localStorage.getItem("userLoggedIn")) {
        next("/");
      }
      next();
    },
  },
  {
    path: "/logout",
    name: "Logout",
    redirect: () => {
      localStorage.removeItem("userLoggedIn");
      return "/login";
    },
  },
  {
    path: "/",
    name: "Home",
    beforeEnter: (to, from, next) => {
      checkUserLoggedIn(next);
    },
    component: Home,
  },
  {
    path: "/product-details/:productCode",
    name: "Product Details",
    beforeEnter: (to, from, next) => {
      checkUserLoggedIn(next);
    },
    component: ProductDetails,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
