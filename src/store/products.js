import axios from "axios";

const state = () => {
  return { products: "" };
};

const mutations = {
  SET_PRODUCTS: function (state, products) {
    state.products = products;
  },
};

const actions = {
  getProductsList: function ({ commit }, payload) {
    axios
      .get(
        "http://localhost:3000/api/getProductsList" +
          (payload && payload.searchText
            ? "?searchText=" + payload.searchText
            : "")
      )
      .then((response) => {
        if (response && response.data) {
          commit("SET_PRODUCTS", response.data);
        }
      });
  },
};
// getters: { ... }

// Export in format that can easily be added as a Vuex module
export default {
  products: {
    // Set modules to namespaced equals true by default
    namespaced: true,
    state,
    actions,
    mutations,
  },
};
