import axios from "axios";

const state = () => {
  return { productBids: "" };
};

const mutations = {
  SET_PRODUCT_BIDS: function (state, productBids) {
    state.productBids = productBids;
  },
};

const actions = {
  getProductBids: function ({ commit }, productCode) {
    axios
      .get(
        "http://localhost:3000/api/getProductBids/" + productCode
      )
      .then((response) => {
        if (response && response.data) {
          commit("SET_PRODUCT_BIDS", response.data);
        }
      });
  },
};

// Export in format that can easily be added as a Vuex module
export default {
  bids: {
    // Set modules to namespaced equals true by default
    namespaced: true,
    state,
    actions,
    mutations,
  },
};
