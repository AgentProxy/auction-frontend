import Vue from "vue";
import Vuex from "vuex";

import bids from "./bids";
import products from "./products";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    ...products,
    ...bids
  },
});
