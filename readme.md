# auction-frontend


### Project setup
1. Run ```npm install``` to install the dependencies for the frontend vue app
2. Run ```npm run dev``` to fire up the app (default port is 8080)
3. Default sample login credentials are ``` First user: user1/user1_pass Second user: user2/user2_pass ```

### Features Implemented
1. Dummy Login and Saving of Logged in Account via localStorage
2. Displaying of products in the home page
3. Search products via name and description
4. Showing of product details
5. Showing of product bids
6. Submitting of bid

### Project Description
This is the front-end for the antique auction app. Vue is the main front-end framework used with Bulma for the CSS framework. The application will allow users to bid on antique items displayed in the site. For simplicity, the endpoints for the requests going to the back-end were hardcoded in this app. 

